#! /usr/bin/env guile
!#

; Write a program to sum up a list

(define (sumlist numbers)
  (if (null? numbers) 0
    (+ (car numbers) (sumlist (cdr numbers)))))

(define example '(1 2 3 4 5 6 7 8 9 10))
(display "The sum of the list ")
(display example)
(display " is ")
(display (sumlist example))
(display ".")
(newline)

