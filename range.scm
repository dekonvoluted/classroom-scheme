#! /usr/bin/env guile
!#

; Print out a list containing integers up to a maximum value

(define (range n)
  (define (recurse counter numList)
    (if (= counter 0) (cons 0 numList)
      (recurse (- counter 1) (cons counter numList))))
  (if (< n 0) '()
    (recurse (- n 1) '())))

(display (range 15))
(newline)

