#! /usr/bin/env guile
!#

; Flatten a list by merging in sub-lists

(define (flatten nestedList)
  (cond ((null? nestedList) '())
        ((list? (car nestedList)) (append (flatten (car nestedList)) (flatten (cdr nestedList))))
        (else (cons (car nestedList) (flatten (cdr nestedList))))))

; Test the function
(display (flatten '(1 2 3 4)))
(newline)
(display (flatten '(1 (2 "3") "4" ((5)))))
(newline)

