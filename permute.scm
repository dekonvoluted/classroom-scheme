#! /usr/bin/env guile
!#

; Print out all permutations of a given set of numbers.
; The set is assumed to be composed of unique elements.
; The set is assumed to be flat.

; Define removing an element from a sequence
(define (remove element sequence)
  (cond ((null? sequence) sequence)
        ((equal? (car sequence) element) (remove element (cdr sequence)))
        (else (cons (car sequence) (remove element (cdr sequence))))))

; Define generating permutations
; Generate permutations of sequence minus each item
; Cons the item in front of each sub-permutation
; Run these two nested steps on each item in the sequence
(define (permute sequence)
  (if (null? sequence)
    (quote (()))
    (apply append
           (map (lambda (element)
                  (map (lambda (sub-permutation)
                         (cons element sub-permutation))
                       (permute (remove element sequence))))
                sequence))))

; Print out each permutation on a separate line
(define echo
  (lambda (element)
    (display element)
    (newline)))

(map echo
     (permute (quote (1 2 3 4 5))))

