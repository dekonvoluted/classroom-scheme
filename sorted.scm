#! /usr/bin/env guile
!#

; Check if a list of numbers is sorted

; If list has a single element, it's already sorted
; If the first two elements are sorted and the rest of the list (minus the first element) is sorted
(define (sortedNumList? numList)
  (or (< (length numList) 2)
      (and (<= (car numList) (cadr numList)) (sortedNumList? (cdr numList)))))

; Tests
(define single (list 1))
(define sortedDouble (list 1 2))
(define unsortedDouble (list 2 1))
(define sortedMultiple (list 1 2 3 3 4 6))
(define unsortedMultiple (list 1 2 4 9 3))

(display "Single list: ")
(display (sortedNumList? single))
(newline)
(display "Sorted double list: ")
(display (sortedNumList? sortedDouble))
(newline)
(display "Unsorted double list: ")
(display (sortedNumList? unsortedDouble))
(newline)
(display "Sorted multiple list: ")
(display (sortedNumList? sortedMultiple))
(newline)
(display "Unsorted multiple list: ")
(display (sortedNumList? unsortedMultiple))
(newline)

