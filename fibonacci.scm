#! /usr/bin/env guile
!#

; Print the nth Fibonacci term

; Recursive Fibonacci function
(define (fibonacci n)
  (cond ((or (= n 0) (= n 1)) n)
        ((> n 1) (+ (fibonacci (- n 1)) (fibonacci (- n 2))))
        (else "... now you're just being naughty!")))

; Output function
(define (run num)
         (display "The ")
         (display num)
         (display (cond ((= num 1) "st")
                        ((= num 2) "nd")
                        ((= num 3) "rd")
                        (else "th")))
         (display " Fibonacci term is ")
         (display (fibonacci num))
         (display ".")
         (newline))

(run 13)
(run 6)
(run 3)
(run 2)
(run 1)
(run 0)
(run -1)

