#! /usr/bin/env guile
!#

; Print out the power set of a given set
; A power set is the set of all possible subsets

; Faster version that uses let
(define (powerset numList)
  (if (null? numList) '(())
    (let ((subList (powerset (cdr numList))))
      (append subList (map (lambda (subset)
                             (cons (car numList) subset))
                           subList)))))


; Test
(display (powerset (list 1 2 3 4 5)))
(newline)

